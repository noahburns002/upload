document.addEventListener('DOMContentLoaded', () => {
    const fileInput = document.getElementById('fileInput');
    const dragDropArea = document.getElementById('dragDropArea');
    const uploadStatus = document.getElementById('uploadStatus');
    const magnetInput = document.getElementById('magnetInput');
    const magnetUploadStatus = document.getElementById('magnetUploadStatus');
  
    // Prevent default behavior when files are dragged over the drop area
    ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
      dragDropArea.addEventListener(eventName, preventDefaults, false);
      document.body.addEventListener(eventName, preventDefaults, false);
    });
  
    // Highlight drop area when files are dragged over
    ['dragenter', 'dragover'].forEach(eventName => {
      dragDropArea.addEventListener(eventName, highlight, false);
    });
  
    // Remove highlight when files are dragged out
    ['dragleave', 'drop'].forEach(eventName => {
      dragDropArea.addEventListener(eventName, unhighlight, false);
    });
  
    // Handle dropped files
    dragDropArea.addEventListener('drop', handleDrop, false);
  
    // Handle file input change
    fileInput.addEventListener('change', handleFileInputChange);
  
    // Function to prevent default behavior
    function preventDefaults(e) {
      e.preventDefault();
      e.stopPropagation();
    }
  
    // Function to highlight drop area
    function highlight() {
      dragDropArea.classList.add('highlight');
    }
  
    // Function to remove highlight
    function unhighlight() {
      dragDropArea.classList.remove('highlight');
    }
  
    // Function to handle dropped files
    function handleDrop(e) {
      const dt = e.dataTransfer;
      const files = dt.files;
  
      if (files.length > 0) {
        handleFileUpload(files);
      }
    }
  
    // Function to handle file input change
    function handleFileInputChange() {
      const files = fileInput.files;
  
      if (files.length > 0) {
        handleFileUpload(files);
      }
    }
  
    // Function to handle file upload
    function handleFileUpload(files) {
      const formData = new FormData();
  
      for (const file of files) {
        formData.append('imageFiles', file);
      }
  
      fetch('/upload', {
        method: 'POST',
        body: formData,
      })
        .then(response => response.text())
        .then(message => {
          uploadStatus.innerHTML = message;
        })
        .catch(error => {
          console.error('Error uploading files:', error);
        });
    }
  
    // Function to handle magnet link upload
    function uploadMagnet() {
      const magnetLink = magnetInput.value.trim();
  
      if (magnetLink !== '') {
        // You can perform the necessary actions for magnet link upload here
        // For example, send a request to the server
        fetch('/uploadMagnet', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ magnetLink }),
        })
          .then(response => response.text())
          .then(message => {
            magnetUploadStatus.innerHTML = message;
          })
          .catch(error => {
            console.error('Error uploading magnet link:', error);
          });
      } else {
        magnetUploadStatus.innerHTML = 'Please enter a valid magnet link.';
      }
    }
  
    // ... existing code ...
  });
  