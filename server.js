const express = require('express');
const multer = require('multer');
const path = require('path');
const fs = require('fs');

const app = express();
const port = 3000;

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'uploads/');
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage: storage });

app.use(express.static(__dirname));
app.use('/uploads', express.static('uploads'));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

// Handle file uploads
app.post('/upload', upload.array('imageFiles', 99999999999), (req, res) => {
  console.log('Received files:', req.files);
  res.send('Files successfully uploaded!');
});

// Handle magnet link uploads
app.post('/uploadMagnet', express.json(), (req, res) => {
  const magnetLink = req.body.magnetLink;

  if (magnetLink) {
    const torrentsFolder = path.join(__dirname, 'torrents');
    
    // Create the 'torrents' folder if it doesn't exist
    if (!fs.existsSync(torrentsFolder)) {
      fs.mkdirSync(torrentsFolder);
    }

    // Save the magnet link to a file in the 'torrents' folder
    const fileName = `${Date.now()}_magnet.txt`;
    const filePath = path.join(torrentsFolder, fileName);

    fs.writeFile(filePath, magnetLink, (err) => {
      if (err) {
        console.error('Error saving magnet link:', err);
        res.status(500).send('Error saving magnet link.');
      } else {
        console.log('Magnet link saved:', fileName);
        res.send('Magnet link successfully saved!');
      }
    });
  } else {
    res.status(400).send('Invalid magnet link.');
  }
});

app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
